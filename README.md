# Scripts for working with libib data


libib's builtin label printer is missing a "genre". So will have to use external
software for making labels, in this case glabels. libib exports CSV and glabels can import it. 
libib only exports an entire library's worth of csv in one file, one each for the bibinfo
and one for the barcode info. I find it easiest just to import the whole
thing and then work with it using sqlite.



# TODO

- &#x2611; make process for done barcodes
- &#x2610; add glabels-batch command
