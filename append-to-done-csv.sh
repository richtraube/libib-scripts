while read line
do
    cut -d"," -f 1
done < <(tail -n +2 "$1")
