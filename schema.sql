CREATE TABLE barcodes (
    barcode integer, 
    dymo integer, 
    condition text, 
    barcode_updated text, 
    authors text, 
    first_name text, 
    last_name text, 
    title text, 
    item_created text, 
    item_updated text, 
    publisher text, 
    publish_date text, 
    pages integer, 
    tags text, 
    groups text, 
    oclc integer, 
    lccn integer, 
    ddc text, 
    lcc text, 
    call_number text
);

CREATE TABLE done (
    barcode integer
);
