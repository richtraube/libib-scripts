/*
Grab a set of fifteen adult fiction titles two times to produce one page of thirty labels,
with two copies of each label, one for the spine and one for the interior.
*/
.mode csv
.headers on
SELECT * FROM barcodes WHERE tags NOT LIKE '%children%' AND tags NOT LIKE '%non%' AND barcode NOT IN (SELECT barcode FROM done) ORDER BY last_name ASC, first_name ASC, title ASC LIMIT 15 OFFSET 0;
.headers off
SELECT * FROM barcodes WHERE tags NOT LIKE '%children%' AND tags NOT LIKE '%non%' AND barcode NOT IN (SELECT barcode FROM done) ORDER BY last_name ASC, first_name ASC, title ASC LIMIT 15 OFFSET 0;

